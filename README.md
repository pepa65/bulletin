# piscreen / bulletin
**Signage with Raspberry Pi**
* **piscreen** - Display a webpage on a screen from a Raspberry Pi
* **bulletin** - Display slides with a top info bar on a screen from a Pi
* Required: sudo apt-get grep sed cron [libraspberrypi-bin(vcgencmd): displ]
  git surf unclutter xdotool php-cli [poppler-utils(pdftocairo): bulletin]
  coreutils(cd mkdir mv cp chmod head ln)
* Repo: https://gitlab.com/pepa65/bulletin
* The browser is started after bootup through `show` by crontab
* The script `show` controls the start & stop of the browser and web server.
  (See `show --help` for all options.)
* The script `refr` can be run to refresh the browser page.
* The script `displ` can be run to turn the monitor on or off [needs vcgencmd].

## piscreen
**Display a webpage on a screen from a Raspberry Pi**
* A specified URL is displayed in the browser on startup.

## bulletin
**Display slides with a top info bar on a screen from a Pi**
* The top bar has logo, title, time and AQI (can be adapted by editing
  `_index.html`)
* The slides can be any pdf that gets uploaded, or a Google Slides URL
* The pdfs can be uploaded at http://SITE/upload.php
* The generation of slides from Google Slides can be triggered at
  http://SITE/rebuild.php
* The SITE is IP:PORT, like: http://192.168.7.2:8888
* Either GSURL needs to be set for a Google Slides presentation,
  or a pdf needs to be uploaded.
* During install, DIMX needs to be set correctly for `_index.html`.
* `show` starts a web server for `web/rebuild.php` and `web/upload.php`.

## Install
### Raspberry OS
Install Raspberry OS. Use `raspi-config` to configure the wireless connection.
Enabling an ssh server is recommended for remote access.

### Screen
For the **bulletin** the `_index.html` file assumes a certain screen size. 
Configure the screen resolution with `raspi-config` (Advanced, Resolution).
If the display needs to be rotated, add a line like this to `/boot/config.txt`:

`display_hdmi_rotate 3`

(Using `3` rotates the display 90 degrees counter-clockwise and `1` clockwise).

If there is overscan (content not aligning well with screen edges), the
`setoverscan` tool can be used from https://github.com/pepa65/setoverscan

### Automated Install
* Download the file `https://gitlab.com/pepa65/bulletin/raw/master/_INSTALL`
  with wget: `wget -qO INSTALL good4.eu/piscreen` and edit the top variables.
* Then the whole manual install can be skipped by doing: `bash INSTALL`

### Manual Install
Studying what happens in INSTALL will give more clues as to what is needed..!

#### Home directory
The assumption is that the user is `pi` with home directory `/home/pi`.
The file `displ` needs to be changed if not!

#### Packages
Install all the required packages:

`apt install git surf unclutter xdotool php-cli libraspberrypi-bin poppler-utils`

#### Download
Clone the git repo:

`mkdir ~/git; cd ~/git; git clone https://gitlab.com/pepa65/bulletin; cd bulletin`

#### Set URL
```
cp _show show
cp _process process
cp _index.html web/index.html
chmod +x process show
```

* Replace `SITE` with `$IP:$PORT` and `URL` with the **piscreen** URL in `show`.
* Replace `GSURL` and `DIMX` in `process`.
* For **bulletin** replace `DIMX` with the screen width in `web/index.html`.

#### Set bootup and screentimes
Add crontab lines to start at bootup and turn the screen on & off at certain times
(see _INSTALL).
